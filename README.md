# Setting up OSX for development#

**Clone this repository**
~~~~
$ git clone https://aadesh@bitbucket.org/zlemmaui/osx-dev-setup.git
~~~~

Now execute the file. This will make sure all the pre-requisites are installed before you clone the core ZLemma git repository
~~~~
$ cd osx-dev-setup
$ chmod u+x install.sh
$ ./install.sh
~~~~

After all the pre-requisites are installed, we need to setup MySQL. Run these commands individually at the command prompt
~~~~
$ unset TMPDIR
$ mkdir /usr/local/var
$ mysql_install_db --verbose --user=`whoami` --basedir="$(brew --prefix mysql)" --datadir=/usr/local/var/mysql --tmpdir=/tmp
~~~~ 
Next, cone the ZLemma repository
~~~~
$ git clone https://yourname@bitbucket.org/zlemma/zlemma.git
~~~~
replace "*yourname*" with your bitbucket username. This will take time to download as it's around 600MB

You now need to create a virtual environment and activate it before proceeding with the installation.
~~~~
$ virtualenv venv
$ source ~/venv/bin/activate
~~~~

Now start the installation process
~~~~
$ cd zlemma
$ pip install -r requirements.txt --extra-index-url=http://chameleon1.zlemma.com:8080/simple/ --download-cache=~/tmp
~~~~
This will ensure that you don’t re-download the requirements again if internet fails.

While the requirements are getting installed, lets setup ipython. Run ipython at your command prompt.
~~~~
$ ipython
~~~~

Once into the iphython shell, run each of these commands individually.
~~~~
ipython> nltk.download('punkt')
ipython> nltk.download('brown')
ipython> nltk.download('stopwords')
ipython> nltk.download('wordnet')
~~~~

After the requirements are installed, install two additional python packages using pip. 
Make sure you are in virtual environment when you install these packages.
~~~~
$ pip install facebook
$ pip install geoip
$ pip install pdfcrowd
~~~~

Once your 
This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact